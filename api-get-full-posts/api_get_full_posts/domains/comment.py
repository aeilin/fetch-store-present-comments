from pydantic.dataclasses import dataclass


@dataclass
class Comment:
    post_id:    int
    id:         int
    name:       str
    email:      str
    body:       str
