from typing import List
from dataclasses import field

from pydantic.dataclasses import dataclass

from api_get_full_posts import domains


@dataclass
class Post:
    user_id:    int
    id:         int
    title:      str
    body:       str

    comments: List[domains.Comment] = field(default_factory=list)
