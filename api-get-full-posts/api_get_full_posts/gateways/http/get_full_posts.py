from typing import List, Mapping
import logging

from marshmallow import schema, fields

from api_get_full_posts import service
from api_get_full_posts.gateways import errors


logger = logging.getLogger(__name__)


class Comment(schema.Schema):
    post_id = fields.Int(data_key='postId')
    id      = fields.Int()
    name    = fields.Str()
    email   = fields.Str()
    body    = fields.Str()


class Post(schema.Schema):
    user_id  = fields.Int(data_key='userId')
    id      = fields.Int()
    title   = fields.Str()
    body    = fields.Str()

    comments = fields.Nested(Comment, many=True)


schema = Post()


def get_full_posts() -> List[Mapping]:
    try:
        domain_posts = service.get_full_posts()
    except:
        logger.error('during get_full_posts acquiring exception happened', exc_info=True)
        raise errors.ServerError from None

    return schema.dump(domain_posts, many=True)
