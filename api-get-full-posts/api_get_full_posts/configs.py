from environs import Env

env = Env()


POSTS_URI       = env.str("POSTS_URI")
COMMENTS_URI    = env.str("COMMENTS_URI")

LOG_LEVEL = env.log_level("LOG_LEVEL", "INFO")
