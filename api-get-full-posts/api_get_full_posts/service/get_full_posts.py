from typing import List
import logging

from api_get_full_posts import (
    domains,
    data,
    service,
)


logger = logging.getLogger(__name__)


def get_full_posts() -> List[domains.Post]:
    try:
        posts = data.http.posts.get()
    except data.errors.DataError as e:
        logger.error('during posts gathering exception occured', exc_info=True)
        raise service.errors.PostsGatheringError from e
    except Exception as e:
        logger.error('during posts gathering unknown exception occured', exc_info=True)
        raise service.errors.ServiceError from e

    try:
        comments = data.http.comments.get()
    except data.errors.DataError as e:
        logger.error('during comments gathering exception occured', exc_info=True)
        raise service.errors.PostsGatheringError from e
    except Exception as e:
        logger.error('during comments gathering unknown exception occured', exc_info=True)
        raise service.errors.ServiceError from e

    try:
        return service.merge(posts, comments)
    except Exception as e:
        logger.error('during merging unknown exception occured', exc_info=True)
        raise service.errors.ServiceError from e
