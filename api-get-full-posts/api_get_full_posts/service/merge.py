from typing import List
import logging

from api_get_full_posts import domains


logger = logging.getLogger(__name__)


def merge(
    posts: List[domains.Post],
    comments: List[domains.Comment]
) -> List[domains.Post]:
    """
        this function uses reference nature of python variables
        posts will be changed during this function call
    """
    id_to_post = {
        post.id: post
        for post
        in posts
    }

    for comment in comments:
        if comment.post_id not in id_to_post:
            logger.warning(
                'comment without a parent post are met',
                extra={
                    'post_id': comment.post_id,
                    'comment_id': comment.id,
                }
            )
            continue

        id_to_post[comment.post_id].comments.append(comment)

    return posts
