from typing import List
import logging

from api_get_full_posts import (
    configs,
    domains,
)
from api_get_full_posts.data import errors
from api_get_full_posts.data.http import queries


logger = logging.getLogger(__name__)


def get() -> List[domains.Post]:
    try:
        data = queries.get(configs.POSTS_URI)
    except:
        logger.error(
            'during posts gathering exception occured',
            extra={'uri': configs.POSTS_URI},
            exc_info=True
        )
        raise

    try:
        return [
            domains.Post(
                user_id=object['userId'],
                id=object['id'],
                title=object['title'],
                body=object['body'],
            )
            for object
            in data
        ]
    except KeyError as e:
        logger.error(
            'during posts parsing exception occured',
            extra={'uri': configs.POSTS_URI, 'data': data},
            exc_info=True
        )
        raise errors.InvalidSchemaError from e
