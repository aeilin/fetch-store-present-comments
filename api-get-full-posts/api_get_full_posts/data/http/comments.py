from typing import List
import logging

from api_get_full_posts import (
    configs,
    domains,
)
from api_get_full_posts.data import errors
from api_get_full_posts.data.http import queries


logger = logging.getLogger(__name__)


def get() -> List[domains.Comment]:
    try:
        data = queries.get(configs.COMMENTS_URI)
    except:
        logger.error(
            'during comments gathering exception occured',
            extra={'uri': configs.COMMENTS_URI},
            exc_info=True
        )

    try:
        return [
            domains.Comment(
                post_id=object['postId'],
                id=object['id'],
                name=object['name'],
                email=object['email'],
                body=object['body']
            )
            for object
            in data
        ]
    except KeyError as e:
        logger.error(
            'during comments parsing exception occured',
            extra={'uri': configs.COMMENTS_URI, 'data': data},
            exc_info=True
        )
        raise errors.InvalidSchemaError from e
