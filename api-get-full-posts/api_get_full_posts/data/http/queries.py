import logging

import requests

from api_get_full_posts.data import errors


logger = logging.getLogger(__name__)


def get(uri: str):
    response = requests.get(uri)

    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        logging.error(
            'during http request exception occured',
            extra={
                'uri': uri,
                'status': response.status_code,
                'data': response.text,
            },
            exc_info=True
        )
        raise errors.ThirdPartyError from e

    try:
        return response.json()
    except ValueError as e:
        logging.error(
            'during json parsing exception occured',
            extra={
                'uri': uri,
                'data': response.text,
            },
            exc_info=True
        )
        raise errors.InvalidSchemaError from e
