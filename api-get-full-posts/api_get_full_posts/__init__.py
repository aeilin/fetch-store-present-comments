import logging
import logging.config

from . import configs

logging.config.fileConfig('logging.ini', disable_existing_loggers=True)
logger = logging.getLogger(__name__)
logger.setLevel(configs.LOG_LEVEL)

from . import errors
from . import domains
from . import data
from . import service
from . import gateways
