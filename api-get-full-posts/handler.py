import json

import api_get_full_posts


DATA = dict()


def handler(event, context):
    global DATA
    if DATA:
        return {
            'statusCode': 200,
            'body': json.dumps(DATA),
        }

    try:
        DATA = api_get_full_posts.gateways.http.get_full_posts()
    except api_get_full_posts.gateways.errors.ServerError:
        # XXX: notification 1
        return {
            'statusCode': 500,
            'body': 'specific error message'
        }
    except:
        # XXX: notification 2
        return {
            'statusCode': 500,
            'body': 'general error message'
        }

    return {
        'statusCode': 200,
        'body': json.dumps(DATA),
    }
