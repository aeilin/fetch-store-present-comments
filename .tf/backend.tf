terraform {
  backend "s3" {
    dynamodb_table = "ops-terraform-lock"

    bucket = "ops-2ca9b506"
    key    = "tfstate/terraform.tfstate"
    region = "us-east-1"
  }
}
