variable "ops_bucket" {}

variable "env" {}

variable "build" {}

variable "region" {
    default = "us-east-1"
}
