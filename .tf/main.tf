module "app" {
  source = "./modules"

  env         = var.env
  ops_bucket  = var.ops_bucket
  build       = var.build
}
