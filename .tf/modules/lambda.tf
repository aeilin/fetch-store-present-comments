resource "aws_cloudwatch_log_group" "api_get_full_posts" {
    name = "/aws/lambda/${var.env}-${local.api_get_full_posts_name}"
}

resource "aws_lambda_function" "api_get_full_posts" {
   function_name = "${var.env}-${local.api_get_full_posts_name}"

   s3_bucket = var.ops_bucket
   s3_key    = "${local.api_get_full_posts_name}-${var.build}.zip"

   handler = "handler.handler"
   runtime = "python3.8"

   role = aws_iam_role.api_get_full_posts.arn

   environment {
       variables = {
            POSTS_URI       = var.posts_uri
            COMMENTS_URI    = var.comments_uri
       }
   }

   depends_on = [
       aws_cloudwatch_log_group.api_get_full_posts
    ]
}

resource "aws_lambda_permission" "api_get_full_posts" {
    statement_id    = "api_get_full_posts"
    action          = "lambda:InvokeFunction"
    function_name   = aws_lambda_function.api_get_full_posts.function_name
    principal       = "apigateway.amazonaws.com"

    source_arn      = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}
