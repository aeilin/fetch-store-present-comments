resource "aws_api_gateway_rest_api" "api" {
  name        = "${var.env}-api"
}

resource "aws_api_gateway_resource" "api_posts" {
   rest_api_id = aws_api_gateway_rest_api.api.id
   parent_id   = aws_api_gateway_rest_api.api.root_resource_id

   path_part = "posts"
}

resource "aws_api_gateway_method" "api_posts" {
   rest_api_id   = aws_api_gateway_rest_api.api.id
   resource_id   = aws_api_gateway_resource.api_posts.id
   http_method   = "GET"
   authorization = "NONE"
}

resource "aws_api_gateway_integration" "api_posts" {
   rest_api_id = aws_api_gateway_rest_api.api.id
   resource_id = aws_api_gateway_resource.api_posts.id
   http_method = aws_api_gateway_method.api_posts.http_method

   integration_http_method = "POST"
   type                    = "AWS_PROXY"
   uri                     = aws_lambda_function.api_get_full_posts.invoke_arn
}

resource "aws_api_gateway_deployment" "api" {
   depends_on = [
     aws_api_gateway_integration.api_posts,
   ]

   rest_api_id = aws_api_gateway_rest_api.api.id
   stage_name  = "${var.env}"
}
