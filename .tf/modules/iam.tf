data "aws_iam_policy_document" "lambda" {
    statement {
        effect = "Allow"
        actions = [
            "sts:AssumeRole",
        ]
        principals {
            type = "Service"
            identifiers = [
                "lambda.amazonaws.com"
            ]
        }
    }
}

data "aws_iam_policy_document" "api_get_full_posts" {
    statement {
        effect = "Allow"
        actions = [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
        ]
        resources = [
            "${aws_cloudwatch_log_group.api_get_full_posts.arn}:*"
        ]
    }
}

resource "aws_iam_role" "api_get_full_posts" {
   name                 = "${var.env}-${local.api_get_full_posts_name}"
   assume_role_policy   = data.aws_iam_policy_document.lambda.json
}

resource "aws_iam_role_policy" "api_get_full_posts" {
    role    = aws_iam_role.api_get_full_posts.id
    policy  = data.aws_iam_policy_document.api_get_full_posts.json
}
