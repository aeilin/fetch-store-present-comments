variable "ops_bucket" {}

variable "build" {}

variable "env" {}

variable "posts_uri" {
  default     = "https://jsonplaceholder.typicode.com/posts"
}

variable "comments_uri" {
    default   = "https://jsonplaceholder.typicode.com/comments"
}
