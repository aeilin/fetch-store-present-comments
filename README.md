# TASK

* There are users, writing posts, provided by a service: https://jsonplaceholder.typicode.com/posts
* There are users, commenting on these posts, provided by a service: https://jsonplaceholder.typicode.com/comments
* Your code will fetch all the posts and comments, store them, and present them in a single structure.
* You are free to make your own decision and use any resource available.
* Bonus point if solution can be triggered/executed by people without Python or Linux knowledge.
* Bonus point if your solution is going to be easy to deploy and scale horizontally, in case we need to monitor many countries/API's.